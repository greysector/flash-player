%global debug_package %{nil}
%global ver 32

Name: flash-player
Summary: Adobe Flash Player
Version: %{ver}.0.0.465
Release: 1
License: Adobe
Group: Applications/Internet
URL: http://www.adobe.com/products/flashplayer/
Source0: https://fpdownload.adobe.com/get/flashplayer/pdc/%{version}/flash_player_npapi_linux.x86_64.tar.gz
Source1: https://fpdownload.adobe.com/pub/flashplayer/pdc/%{version}/flash_player_ppapi_linux.x86_64.tar.gz
Source3: https://helpx.adobe.com/flash-player/release-note/fp_%{ver}_air_%{ver}_release_notes.html
Source4: https://sources.gentoo.org/cgi-bin/viewvc.cgi/gentoo-x86/www-plugins/adobe-flash/files/mms.cfg
Source5: https://www.adobe.com/content/dam/acom/en/devnet/flashplayer/articles/flash_player_admin_guide/pdf/flash_player_%{ver}_0_admin_guide.pdf
ExclusiveArch: x86_64
BuildRequires: desktop-file-utils
Provides: flash-plugin = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}
Requires: hicolor-icon-theme
Requires: libasound.so.2()(64bit)
Requires: libcurl.so.4()(64bit)
Requires: libGL.so.1()(64bit)
Enhances: firefox

%global __provides_exclude ^lib_plugin\\.so()

%global desc Adobe® Flash® Player is a lightweight browser plug-in and rich Internet\
application runtime that delivers consistent and engaging user experiences,\
stunning audio/video playback, and exciting gameplay.\
\
Installed on more than 1.3 billion systems, Flash Player is the standard for\
delivering high-impact, rich Web content.\
\
By downloading and installing this package you agree to the included\
End-User License Agreement:\
\
https://www.adobe.com/legal/licenses-terms.html

%description
%{desc}

%package -n chromium-pepper-flash
Summary: Adobe Flash Player - PPAPI
Requires: %{name}-common = %{version}-%{release}
Enhances: chromium
Enhances: freshplayerplugin

%description -n chromium-pepper-flash
%{desc}

%package common
Summary: Adobe Flash Player configuration and documentation
BuildArch: noarch

%description common
%{desc}

This package contains the global configuration file and documentation.

%prep
%setup -q -c -a 1
install -pm644 %{S:3} %{S:5} ./

%build

%install
install -Dp -m0755 libflashplayer.so \
  %{buildroot}%{_libdir}/mozilla/plugins/libflashplayer.so

install -Dp -m0644 %{SOURCE4} %{buildroot}/etc/adobe/mms.cfg

install -Dp -m0755 usr/bin/flash-player-properties \
  %{buildroot}%{_bindir}/flash-player-properties

for sz in 16 22 24 32 48 ; do
  install -Dp -m644 usr/share/icons/hicolor/${sz}x${sz}/apps/flash-player-properties.png \
     %{buildroot}%{_datadir}/icons/hicolor/${sz}x${sz}/apps/flash-player-properties.png
done

desktop-file-install --dir %{buildroot}%{_datadir}/applications \
  usr/share/applications/flash-player-properties.desktop

install -Dp -m0755 libpepflashplayer.so \
  %{buildroot}%{_libdir}/chromium-browser/PepperFlash/libpepflashplayer.so
install -Dp -m0644 manifest.json \
  %{buildroot}%{_libdir}/chromium-browser/PepperFlash/manifest.json

%files
%{_bindir}/flash-player-properties
%{_libdir}/mozilla/plugins/libflashplayer.so
%{_datadir}/applications/flash-player-properties.desktop
%{_datadir}/icons/hicolor/*/apps/flash-player-properties.png

%files -n chromium-pepper-flash
%dir %{_libdir}/chromium-browser
%{_libdir}/chromium-browser/PepperFlash

%files common
%license license.pdf LGPL/LGPL.txt LGPL/notice.txt
%doc flash_player_%{ver}_0_admin_guide.pdf fp_%{ver}_air_%{ver}_release_notes.html readme.txt
%config(noreplace) /etc/adobe/mms.cfg

%changelog
* Fri Dec 11 2020 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.465-1
- update to 32.0.0.465

* Tue Oct 13 2020 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.445-1
- update to 32.0.0.445
- fixes CVE-2020-9746 (APSB20-58)

* Wed Sep 02 2020 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.414-1
- update to 32.0.0.414

* Tue Jun 09 2020 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.387-1
- update to 32.0.0.387
- fixes CVE-2020-9633 (APSB20-30)

* Mon Apr 13 2020 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.344-1
- update to 32.0.0.344
- filter out invalid Provides:
- weaken weak deps

* Wed Feb 12 2020 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.330-1
- update to 32.0.0.330
- fixes CVE-2020-3757 (APSB20-06)

* Wed Dec 04 2019 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.293-1
- update to 32.0.0.293

* Mon Oct 21 2019 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.270-1
- update to 32.0.0.270

* Wed Sep 11 2019 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.255-1
- update to 32.0.0.255
- fixes CVE-2019-8070 and CVE-2019-8069 (APSB19-46)

* Tue Jun 11 2019 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.207-1
- updated to 32.0.0.207
- fixes CVE-2019-7845 (APSB19-30)

* Wed May 15 2019 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.192-1
- updated to 32.0.0.192
- fixes CVE-2019-7837 (APSB19-26)
- drop dist macro from release tag, package is the same for all Fedora

* Tue Apr 09 2019 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.171-1
- updated to 32.0.0.171
- fixes CVE-2019-7108 and CVE-2019-7096 (APSB19-19)

* Fri Mar 22 2019 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.156-1
- updated to 32.0.0.156

* Tue Feb 12 2019 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.142-1
- updated to 32.0.0.142
- fixes CVE-2019-7090 (APSB19-06)

* Sun Jan 13 2019 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.114-1
- updated to 32.0.0.114 (APSB19-01)

* Fri Dec 07 2018 Dominik Mierzejewski <rpm@greysector.net> - 32.0.0.101-1
- updated to 32.0.0.101
- fixes CVE-2018-15982 and CVE-2018-15983 (APSB18-42)

* Wed Nov 21 2018 Dominik Mierzejewski <rpm@greysector.net> - 31.0.0.153-1
- updated to 31.0.0.153
- fixes CVE-2018-15981 (see APSB18-44)
- include admin guide
- split docs and config into a -common subpackage

* Sun Nov 18 2018 Dominik Mierzejewski <rpm@greysector.net> - 31.0.0.148-1
- updated to 31.0.0.148
- fixes CVE-2018-15978 (see APSB18-39)

* Thu Oct 18 2018 Dominik Mierzejewski <rpm@greysector.net> - 31.0.0.122-1
- updated to 31.0.0.122
- fixes CVE-2018-15967 (see APSB18-31)

* Wed Aug 15 2018 Dominik Mierzejewski <rpm@greysector.net> - 30.0.0.154-1
- updated to 30.0.0.154
- fixes CVE-2018-12824, CVE-2018-12825, CVE-2018-12826, CVE-2018-12827
  and CVE-2018-12828 (see APSB18-25)

* Wed Jul 11 2018 Dominik Mierzejewski <rpm@greysector.net> - 30.0.0.134-1
- updated to 30.0.0.134
- fixes CVE-2018-5007 and CVE-2018-5008 (see APSB18-24)

* Fri Jun 22 2018 Dominik Mierzejewski <rpm@greysector.net> - 30.0.0.113-2
- build both PPAPI and NPAPI plugin packages from one spec

* Fri Jun 08 2018 Dominik Mierzejewski <rpm@greysector.net> - 30.0.0.113-1
- updated to 30.0.0.113 (fixes APSB18-19)

* Tue May 08 2018 Dominik Mierzejewski <rpm@greysector.net> - 29.0.0.171-1
- updated to 29.0.0.171 (fixes APSB18-16)

* Thu Apr 12 2018 Dominik Mierzejewski <rpm@greysector.net> - 29.0.0.140-1
- updated to 29.0.0.140 (fixes APSB18-08)

* Tue Mar 13 2018 Dominik Mierzejewski <rpm@greysector.net> - 29.0.0.113-1
- updated to 29.0.0.113 (fixes APSB18-05)

* Wed Feb 07 2018 Dominik Mierzejewski <rpm@greysector.net> - 28.0.0.161-1
- updated to 28.0.0.161 (fixes APSB18-03)

* Wed Jan 10 2018 Dominik Mierzejewski <rpm@greysector.net> - 28.0.0.137-1
- updated to 28.0.0.137 (fixes APSB18-01)

* Wed Dec 13 2017 Dominik Mierzejewski <rpm@greysector.net> - 28.0.0.126-1
- updated to 28.0.0.128 (fixes APSB17-42)

* Wed Nov 15 2017 Dominik Mierzejewski <rpm@greysector.net> - 27.0.0.187-1
- updated to 27.0.0.187 (fixes APSB17-33)

* Thu Nov 02 2017 Dominik Mierzejewski <rpm@greysector.net> - 27.0.0.183-1
- updated to 27.0.0.183 (fixes APSB17-31 and APSB17-32)
- drop i686 support
- drop ancient Obsoletes:

* Wed Sep 13 2017 Dominik Mierzejewski <rpm@greysector.net> - 27.0.0.130-1
- updated to 27.0.0.130 (fixes APSB17-28)

* Sat Aug 12 2017 Dominik Mierzejewski <rpm@greysector.net> - 26.0.0.151-1
- updated to 26.0.0.151 (fixes APSB17-23)

* Tue Jul 11 2017 Dominik Mierzejewski <rpm@greysector.net> - 26.0.0.137-1
- updated to 26.0.0.137 (fixes APSB17-21)
- updated URL to EULA in description

* Tue Jun 13 2017 Dominik Mierzejewski <rpm@greysector.net> - 26.0.0.126-1
- updated to 26.0.0.126 (fixes APSB17-17)

* Tue May 09 2017 Dominik Mierzejewski <rpm@greysector.net> - 25.0.0.171-1
- updated to 25.0.0.171 (fixes APSB17-15)

* Tue Apr 11 2017 Dominik Mierzejewski <rpm@greysector.net> - 25.0.0.148-1
- updated to 25.0.0.148 (fixes APSB17-10)

* Tue Mar 14 2017 Dominik Mierzejewski <rpm@greysector.net> - 25.0.0.127-1
- updated to 25.0.0.127 (fixes APSB17-07)

* Tue Feb 14 2017 Dominik Mierzejewski <rpm@greysector.net> - 24.0.0.221-1
- updated to 24.0.0.221 (fixes APSB17-04)

* Wed Jan 11 2017 Dominik Mierzejewski <rpm@greysector.net> - 24.0.0.194-1
- updated to 24.0.0.194 (fixes APSB17-02)

* Thu Dec 15 2016 Dominik Mierzejewski <rpm@greysector.net> - 24.0.0.186-1
- updated to 24.0.0.186 (fixes APSB16-39)

* Wed Nov 09 2016 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.644-1
- updated to 11.2.202.644 (fixes APSB16-37)

* Thu Sep 15 2016 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.635-1
- updated to 11.2.202.635 (fixes APSB16-29)

* Thu Jul 14 2016 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.632-1
- updated to 11.2.202.632 (fixes APSB16-25)

* Fri Jun 17 2016 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.626-1
- updated to 11.2.202.626 (fixes APSB16-18)

* Thu May 12 2016 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.621-1
- updated to 11.2.202.621 (fixes APSB16-15)

* Fri Apr 08 2016 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.616-1
- updated to 11.2.202.616 (fixes APSB16-10)

* Fri Mar 11 2016 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.577-1
- updated to 11.2.202.577 (fixes APSB16-08)
- drop redundant defattr and BuildRoot
- use license macro
- specify icon file name explicitly
- cosmetics: break long line, simplify icon installation

* Tue Feb 09 2016 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.569-1
- updated to 11.2.202.569 (fixes APSB16-04)

* Tue Dec 29 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.559-1
- updated to 11.2.202.559 (fixes APSB16-01)

* Tue Dec 08 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.554-1
- updated to 11.2.202.554 (fixes APSB15-32)

* Tue Nov 10 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.548-1
- updated to 11.2.202.548 (fixes APSB15-28)

* Fri Oct 16 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.540-1
- updated to 11.2.202.540 (fixes APSB15-27)

* Tue Oct 13 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.535-1
- updated to 11.2.202.535 (fixes APSB15-25)

* Mon Sep 21 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.521-1
- updated to 11.2.202.521 (fixes APSB15-23)

* Wed Aug 12 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.508-1
- updated to 11.2.202.508 (fixes APSB15-19)

* Fri Jul 24 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.491-2
- add system-wide configuration file from Gentoo
- update Release Notes URL to https
- update EULA links

* Mon Jul 20 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.491-1
- updated to 11.2.202.491 (fixes APSB15-18)

* Wed Jul 08 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.481-1
- updated to 11.2.202.481 (fixes APSB15-16)

* Wed Jun 24 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.468-1
- updated to 11.2.202.468 (fixes APSB15-14)

* Wed Jun 10 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.466-1
- updated to 11.2.202.466 (fixes APSB15-11)

* Wed May 13 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.460-1
- updated to 11.2.202.460 (fixes APSB15-09)

* Wed Apr 15 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.457-1
- updated to 11.2.202.457 (fixes APSB15-06)

* Fri Mar 13 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.451-1
- updated to 11.2.202.451 (fixes APSB15-05)

* Thu Feb 05 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.442-1
- updated to 11.2.202.442 (fixes APSB15-04)
- back to tar.gz as source, no rpms for .442 yet

* Tue Jan 27 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.440-1
- updated to 11.2.202.440 (apparently fixes CVE-2015-0311)
- switch to repackaging upstream RPM package (no .tar.gz with .440)
- reorder sources
- include plain text LICENSE instead of pdf
- drop unnecessary macros and clean section

* Sat Jan 24 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.438-1
- updated to 11.2.202.438 (fixes APSB15-02)

* Wed Jan 14 2015 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.429-1
- updated to 11.2.202.429 (fixes APSB15-01)

* Fri Dec 12 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.425-1
- updated to 11.2.202.425 (fixes APSB14-27)

* Thu Nov 27 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.424-1
- updated to 11.2.202.424 (fixes APSB14-26)
- add readme.txt to doc files

* Sun Nov 16 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.418-1
- updated to 11.2.202.418 (fixes APSB14-24)

* Wed Oct 15 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.411-1
- updated to 11.2.202.411 (fixes APSB14-22)

* Wed Sep 10 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.406-1
- updated to 11.2.202.406 (fixes APSB14-21)

* Wed Aug 13 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.400-1
- updated to 11.2.202.400 (fixes APSB14-18)

* Wed Jul 09 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.394-1
- updated to 11.2.202.394 (fixes APSB14-17)

* Wed Jun 11 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.378-1
- updated to 11.2.202.378 (fixes APSB14-16)

* Wed May 14 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.359-1
- updated to 11.2.202.359 (fixes APSB14-14)

* Tue Apr 29 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.356-1
- updated to 11.2.202.356 (fixes APSB14-13)

* Wed Apr 09 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.350-1
- updated to 11.2.202.350 (fixes APSB14-09)

* Thu Mar 13 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.346-1
- updated to 11.2.202.346 (fixes APSB14-08)

* Fri Feb 21 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.341-1
- updated to 11.2.202.341 (fixes APSB14-07)

* Wed Feb 05 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.336-1
- updated to 11.2.202.336 (fixes APSB14-04)

* Tue Jan 14 2014 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.335-1
- updated to 11.2.202.335 (fixes APSB14-02)

* Tue Dec 10 2013 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.332-1
- updated to 11.2.202.332 (fixes APSB13-28)

* Tue Nov 12 2013 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.327-1
- updated to 11.2.202.327 (fixes APSB13-26)

* Wed Sep 11 2013 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.310-1
- updated to 11.2.202.310 (fixes APSB13-21)
- switched to %%global for macro definitions
- simplified additional deps logic

* Wed Jul 10 2013 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.297-1
- updated to 11.2.202.297 (fixes APSB13-17)

* Wed Jun 12 2013 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.291-1
- updated to 11.2.202.291 (fixes APSB13-16)
- renamed to flash-player
- install the GTK control panel
- drop chrpath and execstack calls (no longer necessary)

* Tue May 14 2013 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.285-1
- updated to 11.2.202.285 (fixes APSB13-14, APSB13-11)

* Mon Apr 08 2013 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.275-1
- updated to 11.2.202.275 (fixes APSB13-09)

* Tue Feb 26 2013 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.273-1
- updated to 11.2.202.273 (fixes APSB13-01, APSB13-04, APSB13-04, APSB13-08)
- updated links

* Wed Dec 12 2012 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.258-1
- updated to 11.2.202.258 (fixes APSB12-27)
- updated EULA link
- dropped beta EULA
- dropped dist from package release string
- updated download links

* Thu Nov 08 2012 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.251-1
- updated to 11.2.202.251

* Fri Aug 17 2012 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.238-1
- updated to 11.2.202.238

* Sun Jun 24 2012 Dominik Mierzejewski <rpm@greysector.net> - 11.2.202.236-1
- updated to 11.2.202.236

* Sun Mar 04 2012 Dominik Mierzejewski <rpm@greysector.net> - 11.1.102.62-1
- updated to 11.1.102.62

* Sun Oct 23 2011 Dominik Mierzejewski <rpm@greysector.net> - 11.0.1.152-1
- updated both versions to 11.0.1.152

* Sat Oct 01 2011 Dominik Mierzejewski <rpm@greysector.net> - 10.3.183.7-6
- updated i686 version to 10.3.183.10

* Tue Sep 20 2011 Dominik Mierzejewski <rpm@greysector.net> - 10.3.183.7-5
- updated x86_64 version to latest rc 11.0.1.129
- updated i686 version to 10.3.183.7

* Thu Jul 14 2011 Dominik Mierzejewski <rpm@greysector.net> - 10.3.181.34-4
- updated x86_64 version to latest beta 10.1.0.60
- updated i686 version to 10.3.181.34

* Wed Mar 23 2011 Dominik Mierzejewski <rpm@greysector.net> - 10.3.162.29-3
- updated i686 version to 10.2.153.1

* Sun Feb 20 2011 Dominik Mierzejewski <rpm@greysector.net> - 10.3.162.29-2
- updated i686 version to 10.2.152.27

* Thu Jan 06 2011 Dominik Mierzejewski <rpm@greysector.net> - 10.3.162.29-1
- updated i686 version to 10.1.102.65
- updated x86_64 version to "Square" Preview 3 64bit (10.3.162.29)

* Tue Sep 21 2010 Dominik Mierzejewski <rpm@greysector.net> - 10.2.161.22-1
- updated i686 version to 10.1.85.3 (fixes APSB10-22)
- updated x86_64 version to "Square" Preview 1 64bit (10.2.161.22)
- updated EULA
- added missing library requirements

* Fri Aug 13 2010 Dominik Mierzejewski <rpm@greysector.net> - 10.1.82.76-1
- updated to 10.1.82.76
- fixes APSB10-16

* Thu Jul 01 2010 Dominik Mierzejewski <rpm@greysector.net> - 10.1.53.64-1
- updated to 10.1.53.64
- fixes APSB10-14

* Sun Feb 14 2010 Dominik Mierzejewski <rpm@greysector.net> - 10.0.45.2-1
- updated to 10.0.45.2
- fixes APSB10-06
- stop pretending the 32bit version works on anything less than i686

* Fri Dec 11 2009 Dominik Mierzejewski <rpm@greysector.net> - 10.0.42.34-1
- updated to 10.0.42.34

* Sun Aug 02 2009 Dominik Mierzejewski <rpm@greysector.net> - 10.0.32.18-1
- updated to 10.0.32.18
- fixes APSB09-10

* Thu Apr 02 2009 Dominik Mierzejewski <rpm@greysector.net> - 10.0.22.87-2
- updated EULA URL

* Wed Feb 25 2009 Dominik Mierzejewski <rpm@greysector.net> - 10.0.22.87-1
- updated to 10.0.22.87
- fixes CVE-2009-0114, CVE-2009-0519, CVE-2009-0520 and CVE-2009-0521

* Sat Dec 20 2008 Dominik Mierzejewski <rpm@greysector.net> - 10.0.21.1-2
- clear execstack bit on the binary

* Thu Dec 18 2008 Dominik Mierzejewski <rpm@greysector.net> - 10.0.21.1-1
- Updated to 10.0.21.1(x86_64)/10.0.15.3(i386)

* Sun Nov 30 2008 Dominik Mierzejewski <rpm@greysector.net> - 10.0.20.7-2
- Added missing Requires: (dlopened)

* Mon Nov 17 2008 Dominik Mierzejewski <rpm@greysector.net> - 10.0.20.7-1
- Add 10.0.20.7 beta for x86_64

* Thu Oct 16 2008 Dominik Mierzejewski <rpm@greysector.net> - 10.0.12.36-1
- Updated to 10 final

* Thu Aug 28 2008 Dominik Mierzejewski <rpm@greysector.net> - 10.0.0.569-1
- Updated to 10 beta build 569
- Use EULA from Adobe's website
- Fix summary and description

* Thu May 15 2008 Dominik Mierzejewski <rpm@greysector.net> - 10.0.0.218-1
- Updated to 10 beta build 218

* Thu Dec 20 2007 Dominik Mierzejewski <rpm@greysector.net> - 9.0.115.0-1
- Updated to 9.0.115.0 (security update)

* Tue Oct 23 2007 Dominik Mierzejewski <rpm@greysector.net> - 9.0.48.0-1
- Updated to 9.0.48.0

* Fri Jun 15 2007 Dominik Mierzejewski <rpm@greysector.net> - 9.0.31.0-2
- Updated to update3 beta.

* Tue Feb 20 2007 Dominik Mierzejewski <rpm@greysector.net> - 9.0.31.0-1
- Updated to release.

* Mon Nov 27 2006 Dominik Mierzejewski <rpm@greysector.net> - 9.0.21.78-1
- Updated to beta2.

* Fri Oct 20 2006 Dominik Mierzejewski <rpm@greysector.net> - 9.0.21.55-1
- Updated to latest beta.
- Renamed to flash-plugin.

* Wed Mar 15 2006 Dag Wieers <dag@wieers.com> - 7.0.63-1 - 3737+/dag
- Updated to release 7.0.63.

* Sat Nov 26 2005 Dag Wieers <dag@wieers.com> - 7.0.61-1
- Updated to release 7.0.61.

* Sun Jun 27 2004 Dag Wieers <dag@wieers.com> - 7.0.25-1
- Initial package. (using DAR)
